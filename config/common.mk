# Local F-droid repo
$(call inherit-product, prebuilts/superitman/fdroid/fdroid-repo.mk)

# Apps
PRODUCT_PACKAGES += \
    Etar \
    ExactCalculator \
    F-Droid \
    F-DroidPrivilegedExtension \
    Nextcloud

